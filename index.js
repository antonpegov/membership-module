var Authentication = require('./lib/authentication');
var Registration = require("./lib/registration");
var Connection = require('./lib/connection');
var User = require('./models/user');
var events = require("events");
var util = require("util");
var mongoose = global.mongoose;

var Membership = function () {

    var self = this;
    events.EventEmitter.call(self);

    self.findUserByToken = function(token,next){
        User.findOne({authenticationToken:token},next);
    };
    self.authenticate = function (email,password, next) {
        var auth = new Authentication();
        //auth.on("not-authenticated",function(authResult){
        //    mongoose.connection.close();
        //    self.emit("not-authenticated", authResult)
        //});
        //auth.on("authenticated",function(authResult){
        //    mongoose.connection.close();
        //    self.emit("authenticated", authResult)
        //});
        auth.applyForAuthentication({email:email,password:password}, next);
    };
    self.register = function(email,password,confirm, next){
        var reg = new Registration();
        //reg.on("not-registered",function(regResult){
        //    mongoose.connection.close();
        //    self.emit("not-registered", regResult)
        //});
        //reg.on("registered",function(regResult){
        //    mongoose.connection.close();
        //    self.emit("registered",regResult)
        //});
        reg.applyForMembership({email: email, password: password, confirm: confirm}, next);
    };
    self.connect = function(user,token,refreshToken,profile,next){

        var con = new Connection();
        //con.on("connected",function(conResult){
        //    console.log("== BOOM! ==".blue,conResult);
        //    self.emit("connected",conResult);
        //});
        //con.on("not-connected",function(conResult){
        //    self.emit("not-connected",conResult);
        //});
        con.connectSocialNetwork({
            'profile': profile,
            'token': token,
            'refreshToken': refreshToken,
            'user': user
        },next);

    };

    return self;
};
util.inherits(Membership, events.EventEmitter);
module.exports = Membership;