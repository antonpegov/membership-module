var assert = require("assert");
var mongoose = global.mongoose;

var logSchema = mongoose.Schema({
    subject: String,
    provider: String,
    entry: String,
    userId: String,
    createdAt: Date
},{collection:'users_log'});

logSchema.statics.newRecord = function(args) {

    assert.ok(args.subject && args.entry && args.userId,
        "Нужны заголовок,текст и идентификатор пользователя");
    var log = {};
    log.subject = args.subject;
    log.provider = args.provider || '';
    log.entry = args.entry;
    log.userId = args.userId;
    log.createdAt = new Date;

    return (new this(log));
};

module.exports = mongoose.model('log',logSchema);