var mongoose = global.mongoose;
var bcrypt = require("bcrypt-nodejs");
var utility = require("../lib/utility");

var userSchema = mongoose.Schema({
    email: String,
    role: String,
    status: String,
    signInCount: Number,
    authenticationToken: String,
    createdAt: {type: Date, default: Date.now},
    currentLoginAt: {type: Date, default: Date.now},
    lastLoginAt: {type: Date, default: Date.now},
    hashedPassword: String,

    facebook         : {
        id           : String,
        token        : String,
        refToken     : String,
        displayName  : String,
        username     : String,
        _json        : String
    },
    twitter          : {
        id           : String,
        token        : String,
        refToken     : String,
        displayName  : String,
        username     : String,
        _json        : String
    },
    google           : {
        id           : String,
        token        : String,
        refToken     : String,
        displayName  : String,
        username     : String,
        _json        : String
    },
    vkontakte           : {
        id           : String,
        token        : String,
        refToken     : String,
        displayName  : String,
        username     : String,
        _json        : String
    }

},{collection:'users'});

// methods ======================
userSchema.statics.newUser = function newUser(args){
    // статическая функция для инициализации при создании
    var user = {};
    user.email = args.email || 'chi-my@yandex.ru';
    user.role = args.role || 'user';
    user.authenticationToken =
        args.authenticationToken || utility.randomString(18);
    user.createdAt = args.createdAt || new Date();
    user.status = args.status || "ожидание";
    user.signInCount = args.signInCount || 0;
    user.currentLoginAt = args.currentLoginAt || new Date;
    user.lastLoginAt = args.lastLoginAt || new Date;
    user.hashedPassword = args.hashedPassword || null;
    return(new this(user));
};
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.methods.validatePassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('users',userSchema);
