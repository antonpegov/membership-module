describe("Регистрация пользователя из Социальной Сети", function () {
    var user = {
        createdAt: 'Fri Feb 05 2016 17:30:48 GMT+0400',
        currentLoginAt: 'Fri Feb 05 2016 17:30:4',
        lastLoginAt: 'Fri Feb 05 2016 17:30:4',
        facebook: {},
        twitter: {},
        google: {},
        vkontakte: {},
        __v: 0,
        hashedPassword: '$2a$10$FshU9tuWCb/eKedFQqCRcul95XjxBTC5JjP4akaaO4OdcIifwXwJK',
        signInCount: 1,
        status: 'approved',
        authenticationToken: 'LLbV10XOodlT5iaGsP',
        role: 'user',
        email: '3@1.1',
        _id: 4083
    };
    var profile_google = { id: '105049814411761708486',
        displayName: 'Антон Пегов',
        name: { familyName: 'Пегов', givenName: 'Антон' },
        emails: [ { value: 'antonpegov@gmail.com', type: 'account' } ],
        photos: [ { value: 'https://lh3.gAAB6k/agSWNVKK1Ow/photo.jpg?sz=50' } ],
        gender: 'male',
        provider: 'google',
        _json:
            { kind: 'plus#person',
                etag: '"4OZ_Kt6ujOh1jaML_U6RM6APqoE/AfCu95I82g0WwT_lV5M4Tdpam7I"',
                occupation: 'живу',
                gender: 'male',
                emails: [ [Object] ],
                objectType: 'person',
                isPlusUser: true,
                language: 'ru',
                circledByCount: 0,
                verified: false }
    };
    var profile_facebook = { id: '10205455319291124',
        username: undefined,
        displayName: 'Anton Pegov',
        name:
        { familyName: undefined,
            givenName: undefined,
            middleName: undefined },
        gender: undefined,
        profileUrl: undefined,
        provider: 'facebook',
        _raw: '{"name":"Anton Pegov","id":"10205455319291124"}',
        _json: { name: 'Anton Pegov', id: '10205455319291124' }
    };
    var profile_twitter = { id: '580207915',
        username: 'antonpegov',
        displayName: 'Anton',
        photos: [ { value: 'https://pbs.twimg.com/profile_images/2262830373/z1_normal.jpg' } ],
        provider: 'twitter',
        _json:
        { id: 580207915,
            id_str: '580207914565',
            name: 'Anton',
            screen_name: 'anton',
            location: 'Kaliningrad',
            description: '',
            url: null,
            entities: { description: [Object] },
            protected: false,
            followers_count: 8
        }
    };
    var profile_vkontakte = {
        id: 11905165,
        username: 'antonpegov',
        displayName: 'Антон Пегов',
        name: {
            familyName: 'Пегов',
            givenName: 'Антон'
        },
        gender: 'male',
        profileUrl: 'http://vk.com/antonpegov',
        photos: [
            {
                value: 'https://pp.vk.me/c306212/v306212165/d213/WUfbGmsKqXA.jpg',
                type: 'photo'
            }
        ],
        provider: 'vkontakte',
        _raw: '{"response":[{"id":11905165,"first_name":"Антон",' +
        '"last_name":"Пегов","sex":2,"screen_name":"antonpegov",' +
        '"photo":"https:\\/\\/pp.vk.me\\/c306212\\/v306212165\\/d213\\/WUfbGmsKqXA.jpg"}]}',
        _json: {
            id: 11905165,
            first_name: 'Антон',
            last_name: 'Пегов',
            sex: 2,
            screen_name: 'antonpegov',
            photo: 'https://pp.vk.me/c306212/v306212165/d213/WUfbGmsKqXA.jpg'
        }
    };
    var token = '12345678';
    var refreshToken = '87654321';
    // Проблемма с дублированием регистрации из социальных сетей
    describe("Вход через уже зарегестрированный аккаунт", function () {
        it("Успешно? Да");
        it("Увеличивает счетчик входов");
        it("Делает запись в лог: 'SN_Authorization'")
    });
    describe("Удачная регистрация - новый пользователь", function () {
        it("Успешно? Да");
        it("Создаёт нового пользователя в БД");
        it("Делает запить в лог: 'SN_Registration' ");
        it("Устанавливает счётчик посещений в 1");
        it("Устанавливает статус пользователя в 'approved'");
    });
    describe("Удачное подключение соцсети к существующему аккаунта",function(){
        it("Успешно? Да");
        it("Обновляет данные существующего пользователя");
        it("Делает запить в лог: SN_Connection");
        it("Увеличивает счётчик посещений");
        it("Обновляет даты посещений");
    });
    describe("Неудачная регистрация - id из соцсети уже зарегистрирован на другом пользователе", function () {
        it("Успешо? Да с оговоркой");
        it("Оставляет всё без изменений");
        it("Просит удалить один из аккаунтов")
    });
    describe("Неудачная регистрация - незнакомая социальная сеть", function () {
        it("Не регистрирует");
        it("");
        it("Предлагает попробовать другую соцеть");
    });

});