var User = require("../models/user");
var should = require("should");

describe("User",function(){
    describe("defaults", function () {
        var user = {};
        before(function(){
            user = User.newUser({email:"chi-my@yandex.ru"});
        });
        it("Почтовый адрес chi-my@yandex.ru", function(){
            user.email.should.equal("chi-my@yandex.ru");
        });
        it("Имеет регистрационный ключ", function(){
            user.authenticationToken.should.be.defined;
        });
        it("Имеет статус --ожидание--", function(){
            user.status.should.be.equal("ожидание");
        });
        it("Имеет дату создания", function(){
            user.createdAt.should.be.defined;
        });
        it("Имеет счётчик сессий равный 0", function(){
            user.signInCount.should.equal(0);
        });
        it("Имеет дату входа", function(){
            user.currentLoginAt.should.be.defined;
        });
        it("Имеет дату последнего входа", function(){
            user.lastLoginAt.should.be.defined;
        });
    });
});
