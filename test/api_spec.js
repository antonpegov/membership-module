var Membership = require("../index");
var config = require("../lib/config");
var mongoose = require("mongoose");
var assert = require("assert");
var should = require("should");
var User = require("../models/user");

describe("API модуля 'Пользователи'", function () {
    var memb = {};
    before(function(done){
        mongoose.connect(config.db);// подключение к БД
        var db = mongoose.connection;
        db.on("error", console.error.bind(console,"Could not connect to mongoDB:"));
        db.once('open',function(){
        User.remove({},function (err,res){
            console.log("Предварительно удаляю записи из базы: %s шт.", res.result.n);
            mongoose.disconnect();
            done();
        });});
    });// почистить базу
    describe("аутентификация", function () {
        var newUser = {};
        before(function(done){
            //подать заявку на регистрацию
            memb = new Membership();
            memb.register('test@test.com','test','test',function(err,result){
                assert.ok(err === null, err);
                newUser = result.user;
                assert.ok(result.success,"Can't register");
                done();
            });
        });
        it("аутентифицирует",function(done){
            memb.authenticate('test@test.com','test',function(err,result){
                result.success.should.be.equal(true);
                done();
            });
        });
        it("находит по токену", function(done){
            memb.findUserByToken(newUser.authenticationToken,function(err,result){
                done();
            });

        });
    });
    afterEach(function(done){
        mongoose.models = {};
        mongoose.modelSchemas = {};
        mongoose.connection.close();
        mongoose.disconnect();
        done();
    });
});