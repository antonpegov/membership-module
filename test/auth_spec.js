var config = require("../lib/config");
var mongoose = require("mongoose");
var assert = require("assert");
var should = require("should");
var Auth = require("../lib/authentication");
var Registration = require("../lib/registration");
var Pumy = mongoose.model('Pumy',new mongoose.Schema(
    {email:String},{collection:'users'}));

describe("Аутентификация", function () {
    var auth = {};
    var reg = {};
    before(function(done){
        auth = new Auth();// создаю объект Авторизация
        reg = new Registration();// создаю объект Регистрация
        mongoose.connect(config.db);// подключение к БД
        var db = mongoose.connection;
        db.on("error", console.error.bind(console,"Could not connect to mongoDB:"));
        Pumy.remove({email:'test@test.com'},function (err,res){
            assert(err === null,err);
            console.log("Предварительно удаляю записи из базы: %s шт.", res.result.n);
            reg.applyForMembership({
                email:'test@test.com',
                password:'test',
                confirm:'test'
            },function(err,result){
                assert(err === null,err);
                done();
            });
        });
    });
    describe("Успешная аутентификация", function () {
        var authResult = {};
        before(function (done) {
            auth.applyForAuthentication({
                email:'test@test.com',
                password:'test',
                confirm:'test'
            },function(err,result){
                assert.ok(err === null,err);
                authResult = result;
                done();
            });
        });
        it("Вход успешный? Да",function(){
            authResult.success.should.be.equal(true);    
        });
        it("Получает объект User", function(){
            authResult.user.should.be.defined
        });
        it("Делает запись в лог", function(){
            authResult.log.should.be.defined
        });
        it("Увеличивает счетчик входов на 1", function(){
            authResult.user.signInCount.should.be.equal(2)
        });
        it("Устанавливает статус 'в системе'",function(){
            authResult.user.status.should.be.equal('logged in')
        });
        it("Устанавливает дату текущего входа",function(){
            authResult.user.currentLoginAt.should.be.defined
        });
        it("Устанавливает дату предыдущего входа", function(){
            authResult.user.currentLoginAt.should.be.above(authResult.user.lastLoginAt)
        });
        it("Обновляет данные пользователя в БД", function(){
            authResult.success.should.be.equal(true)
        });
    });
    describe("Пустой адрес эл.почты", function () {
        var authResult = {};
        before(function (done) {
            auth.applyForAuthentication({
                email:'',
                password:'test',
                confirm:'test'
            },function(err,result){
                assert(err === null,err);
                authResult = result;
                done();
            });
        });
        it("Вход успешный? Нет",function(){
            authResult.success.should.be.equal(false);
        });
        it("Сообщает о необходимости ввести данные",function(){
            authResult.message.should.be.equal('Email and Password are required');
        });
    });
    describe("Пустой пароль", function () {
        var authResult = {};
        before(function (done) {
            auth.applyForAuthentication({
                email:'test@test.com',
                    password:'',
                    confirm:''
            },function(err,result){
                assert(err === null,err);
                authResult = result;

                done();
            });
        });
        it("Вход успешный? Нет",function(){
            authResult.success.should.be.equal(false);
        });
        it("Сообщает о необходимости ввести данные", function(){
            authResult.message.should.be.equal("Email and Password are required");
        });
    });
    describe("Логин или пароль не правильные", function () {
        var authResult = {};
        before(function (done) {
            auth.applyForAuthentication({
                email:'test@test.com',
                password:'t',
                confirm:'t'
            },function(err,result){
                assert(err === null,err);
                authResult = result;
                done();
            });
        });
        it("Вход успешный? Нет",function(){
            authResult.success.should.be.equal(false);
        });
        it("Просит проверить данные и повторить ввод",function(){
            authResult.message.should.be.equal("Wrong login or password")
        });
    });
    after(function(done){
        mongoose.models = {};
        mongoose.modelSchemas = {};
        mongoose.connection.close();
        mongoose.disconnect();
        done();
    });
});