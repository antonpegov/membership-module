var Registration = require('../lib/registration');
var config = require("../lib/config");
var mongoose = require("mongoose");
var assert = require("assert");
var should = require("should");
var Dummy = mongoose.model('Dummy',new mongoose.Schema(
    {email:String},{collection:'users'}));

describe("Регистрация",function(){
    var reg = {};
    before(function(done){
        reg = new Registration();
        mongoose.connect(config.db);
        var db = mongoose.connection;
        db.on("error", console.error.bind(console,"Could not connect to mongoDB:"));
        Dummy.remove({email:'chi-my@yandex.ru'},function (err,res){
            //if(res.n >0 )
            //console.log("Предварительно удаляю записи из базы: %s шт.", res.result.n);
            done();
        });
    });
    describe("Успешная регистрация",function(){
        var regResult = {};
        before(function(done){
            reg.applyForMembership({
                email:'chi-my@yandex.ru',
                password:'123123',
                confirm:'123123'
            },function(err,result){
                assert(err == null,err);
                regResult = result;
                done();
            });

        });
        it("Регистрация успешная? Да", function(){
            regResult.success.should.be.equal(true);
        });
        it("Создаёт нового пользователя", function () {
            regResult.user.should.be.defined;
        });
        it("Делает запись в Лог", function () {
            regResult.log.should.be.defined;
        });
        it("Присваивает пользователю статус 'approved'", function () {
            regResult.user.status.should.be.equal('approved');
        });
        it("Выводит приветственную запись", function () {
            regResult.message.should.be.equal('Welcome!');
        });
        it("Увеличивает счётчик входов пользователя до 1", function () {
            regResult.user.signInCount.should.be.equal(1);
        });
    });
    describe("Пустой адрес эл.почты", function(){
        var regResult = {};
        before(function(done){
            reg.applyForMembership({
                email:'',
                password:'123123',
                confirm:'123123'
            },function(err,result){
                assert(err == null,err);
                regResult = result;
                done();
            });

        });
        it("Регистрация успешная? Нет",function(){
            regResult.success.should.be.equal(false);
        });
        it("Сообщает о необходимости ввести адрес эл.почты", function(){
            regResult.message.should.be.equal("Email and Password are required");
        });
    });
    describe("Пустой пароль", function () {
        var regResult = {};
        before(function(done){
            reg.applyForMembership({
                email:'chi-my@yandex.ru',
                password:'',
                confirm:''
            },function(err,result){
                assert(err == null,err);
                regResult = result;
                done();
            });

        });
        it("Регистрация успешная? Нет", function(){
            regResult.success.should.be.equal(false);
        });
        it("Сообщает о необходимости ввести пароль", function(){
            regResult.message.should.be.equal('Email and Password are required');
        });
    });
    describe("Пароли не совпадают", function () {
        var regResult = {};
        before(function(done){
            reg.applyForMembership({
                email:'chi-my@yandex.ru',
                password:'123',
                confirm:'123123'
            },function(err,result){
                assert(err == null,err);
                regResult = result;
                done();
            });

        });
        it("Регистрация успешная? Нет",function(){
            regResult.success.should.be.equal(false);
        });
        it("Сообщает пользователю о неправильном вводе", function(){
            regResult.message.should.be.equal("Confirmation do not match");
        });
    });
    describe("Адрес уже занят", function () {
        var regResult = {};
        before(function(done){
            var existingUser = new Dummy({email:'chi-my@yandex.ru'});
            existingUser.save(function (err){
                assert(err == null,err);
                reg.applyForMembership({
                    email:'chi-my@yandex.ru',
                    password:'123123',
                    confirm:'123123'
                },function(err,result){
                    assert(err == null,err);
                    regResult = result;
                    done();
                });
            });
        });
        it("Регистрация успешная? Нет.", function(){
            regResult.success.should.be.equal(false);
        });
        it("Просит ввести другой адрес", function(){
            regResult.message.should.be.equal('Email already exists');
        });
    });
    after(function(done){
        mongoose.models = {};
        mongoose.modelSchemas = {};
        mongoose.connection.close();
        mongoose.disconnect();
        done();
    });
});