var dbURI    = require("../lib/config").db
  , should   = require('chai').should()
  , mongoose = require('mongoose')
  , Pummy    = mongoose.model('Pummy', new mongoose.Schema({a:Number}))
//, clearDB  = require('../index')(dbURI)
// Normally, this is:
  , clearDB  = require('mocha-mongoose')(dbURI)
;

describe("Проверка связи с Базой Данных", function() {

  beforeEach(function(done) {
    if (mongoose.connection.db) return done();
    mongoose.connect(dbURI, done);
  });
  it("Может сохранять данне", function(done) {
    new Pummy({a: 1}).save(done);
  });
  it("Может загружать данные", function(done) {
    new Pummy({a: 1}).save(function(err, model){
      if (err) return done(err);

      new Pummy({a: 2}).save(function(err, model){
        if (err) return done(err);

        Pummy.find({}, function(err, docs){
          if (err) return done(err);

          // without clearing the DB between specs, this would be 3
          docs.length.should.equal(2);
          done();
        });
      });
    });
  });
  it("Может очищать при необходимости", function(done) {
    new Pummy({a: 5}).save(function(err, model){
      if (err) return done(err);

      clearDB(function(err){
        if (err) return done(err);

        Pummy.find({}, function(err, docs){
          if (err) return done(err);

          docs.length.should.equal(0);
          done();
        });
      });
    });
  });
});
