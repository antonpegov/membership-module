var User = require("../models/user");
var Application = require("../models/Application");
var Log = require("../models/Log");
var bcrypt = require("bcrypt-nodejs");
var assert = require("assert");
var events = require("events");
var util = require("util");

var Authentication = function () {

    var self = this;
    var continueWith = null;
    events.EventEmitter.call(self);
    var AuthResult = function () {
        var result = {
            success : null,
            message: null,
            user : null,
            log: null
        };
        return result;
    };
    var validateInputs = function (app) {
        if (!app.email||!app.password){
            app.setInvalid("Email and Password are required");
            self.emit("invalid",app);
        } else {
            app.validate("Ok");
            self.emit("validated",app);
        }
    };// Проверка введённых данных
    var checkIfExist = function (app) {
        //mongoose.connection.on('open',function(){// ВЕШАЕТ !!!
        User.findOne({email:app.email},function(err,user){
            assert(err === null,err);
            if (user) {// Пользователь найден
                app.user = user;
                self.emit('user-exist',app);
            } else {// Пользователь не зарегистрирован
                app.setInvalid('Wrong login or password');
                self.emit('invalid',app);
            }
        });
        //})
    };// Проверка наличия адреса
    var authenticate = function(app){
        var matched = bcrypt.compareSync(app.password, app.user.hashedPassword);
        if (matched){
            app.user.status = "logged in";
            app.user.signInCount += 1;
            app.user.lastLoginAt = app.user.currentLoginAt;
            app.user.currentLoginAt = Date.now();
            //app.user.save...
            self.emit('user-authenticated',app);
        } else {
            app.setInvalid('Wrong login or password');
            self.emit('invalid',app);
        }
    };// Проверка соответствия пароля
    var addLogEntry = function(app){
        var log = Log.newRecord({
            subject: "Authentication",
            userId: app.user.id,
            entry: "Пользователь вошел"
        });
        log.save(function (err) {
            assert.ok(err === null, err);
            app.log = log;
            self.emit("log-created",app);
        });
    };// Новая запись в логе
    var authenticationOk = function(app){
        app.user.save(function(err,response){// Сохранить изменения в БД
            var authResult = new AuthResult();
            authResult.user = app.user;
            authResult.log = app.log;
            authResult.success = true;
            authResult.message = "Welcome!";
            self.emit("authenticated",authResult);// сигнал наружу!!!
            if (continueWith){
                continueWith(null,authResult);
            }
        });
    };// Возврат с подтверждением
    var authenticationNotOk = function (app) {
        var authResult = new AuthResult();
        authResult.user = app.user;
        authResult.log = app.log;
        authResult.success = false;
        authResult.message = app.message;
        self.emit("not-authenticated",authResult);// сигнал наружу!!!
        if (continueWith){
            continueWith(null,authResult);
        }
    };// Возврат с отказом
    self.applyForAuthentication = function(args,next){
        continueWith = next;
        var app = new Application(args);
        self.emit('application-received',app)
    };
    self.on("application-received", validateInputs);
    self.on("validated", checkIfExist);
    self.on("user-exist", authenticate);
    self.on("user-authenticated", addLogEntry);
    self.on("log-created",authenticationOk);
    self.on("invalid",authenticationNotOk);
    return self;
};
util.inherits(Authentication, events.EventEmitter);
module.exports = Authentication;