//connection.js
var Log = require("../models/Log");
var User = require("../models/user");
var utility = require("../lib/utility");
var Emitter = require("events").EventEmitter;
var assert = require("assert");
var util = require("util");

var Connection = function(){
    //console.log("connection created...");
    //console.log("== BOOM! ==".blue, global.db.connection.readyState);
    var self = this;
    Emitter.call(this);
    var continueWith = null;
    var networks = ['facebook','twitter','google','vkontakte'];
    var ConResult = function(){
        var result = {
            provider: null,
            success: false,
            message: null,
            user: null,
            log: null
        };
        return result;
    };
    var isInArray = function (value, array) {
        return array.indexOf(value) > -1;
    };
    var validateProvider = function(app){
        if(isInArray(app.profile.provider,networks))
            self.emit("provider-found", app);
        else {
            app.message = "unknown social network";
            self.emit("invalid",app);
        }
    };// проверка поддкржки соцсети
    var findAndUpdate = function (app) {
        var provider = app.profile.provider;
        var query={};
        query[ provider + '.id' ] = app.profile.id;
        if(!app.user) {// человек не залогинен
            User.findOne(query, function (err, record) {
                if (err) return done(err);
                if (record) {//если уже зарегистрирован, обн.статистику
                    app.user = record;
                    app.user.signInCount += 1;
                    app.user.lastLoginAt = app.user.currentLoginAt;
                    app.user.currentLoginAt = Date.now();
                    app.logEntry = "SN_Authorization";
                    self.emit("user-updated",app);
                } else {// если нет, регистрируем
                    var user = User.newUser(app);
                    // set all of the user data that we need
                    user.email = null;
                    user[provider].id = app.profile.id;
                    user[provider].token = app.token;
                    user[provider].refreshToken = app.refreshToken;
                    if(provider !== 'google')
                        user[provider].username = app.profile.username;
                    else user[provider].username = app.profile.emails[0].value;
                    user[provider].displayName = app.profile.displayName;
                    user.status = 'approved';
                    user.signInCount = 1;
                    app.user = user;
                    app.logEntry = "SN_Registration";
                    self.emit("user-updated",app);
                }
            });
        } else {// человек в системе, добавляем ему поля SN
            // проверка отсутствия такого же sn_id у других пользователей
            User.findOne(query, function (err, record) {
                assert.ok(err === null);
                if (record) {
                    app.message = "account already registered";
                    app.logEntry = "SN_Connection_Failed";
                    self.emit("user-updated", app);
                } else {// если всё чисто, добавляем данные
                    app.user[provider].id = app.profile.id;
                    app.user[provider].token = app.token;
                    app.user[provider].refreshToken = app.refreshToken;
                    if (provider !== 'google')
                        app.user[provider].username = app.profile.username;
                    else app.user[provider].username = app.profile.emails[0].value;
                    app.user[provider].displayName = app.profile.displayName;
                    app.logEntry = "SN_Connection";
                    self.emit("user-updated", app);
                }
            });
        }
    };// нашел пользователя и проапгрейдил
    var saveUser = function (app) {
        app.user.save(function(err){
            if (err) throw err;
            self.emit("user-saved",app);
        })
    };
    var addLogEntry = function (app) {
        var log = Log.newRecord({
            subject: app.logEntry,
            userId: app.user.id,
            provider: app.profile.provider,
            entry: "Пользователь зарегистрирован"
        });
        if(app.logEntry == "SN_Connection_Failed")
            log.entry = "Дублирующийся аккаунт соцсети";
        log.save(function (err) {
            assert.ok(err === null, err);
            app.log = log;
            self.emit("log-created",app);
        });
    };
    var addErrorLogEntry = function (app) {
        var log = Log.newRecord({
            subject: "error",
            userId: app.user.id,
            provider: app.profile.provider,
            entry: app.message
        });
        log.save(function (err) {
            assert.ok(err === null, err);
            app.log = log;
            self.emit("error-log-created",app);
        });
    };
    var connectionNotOk = function(app){
        var conResult = new ConResult();
        conResult.success = false;
        conResult.message = app.message;
        conResult.log = app.log;
        conResult.user = app.user;
        self.emit("not-connected",conResult);// сигнал наружу!!!
        if (continueWith){
            continueWith(null,conResult);
        }
    };
    var connectionOk = function(app){
        //console.log("== finising ==".blue,app);
        var conResult = new ConResult();
        conResult.success = true;
        conResult.message = "Welcome!";
        conResult.log = app.log;
        conResult.user = app.user;
        self.emit("connected",conResult);// сигнал наружу!!!
        if (continueWith){
            continueWith(null,conResult);
        }
    };

    self.connectSocialNetwork = function(args,next){
        // принимает profile,token,refToken,user
        args.message = null;
        args.log = null;
        continueWith = next;
        //console.log("== connected from Connection ==".blue);
        self.emit("connection", args);

    };

    self.on("connection",validateProvider);
    self.on("provider-found", findAndUpdate);
    self.on("user-updated", saveUser);
    self.on("user-saved", addLogEntry);
    self.on("log-created",connectionOk);
    self.on("error-log-created",connectionNotOk);
    self.on("invalid",addErrorLogEntry);

    return self;
};
util.inherits(Connection,Emitter);
module.exports = Connection;
//