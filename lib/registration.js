var User = require("../models/user");
var Application = require("../models/Application");
var Log = require("../models/Log");
var bcrypt = require("bcrypt-nodejs");
var assert = require("assert");
var Emitter = require("events").EventEmitter;
var util = require("util");

var Registration = function(){

    var self = this;
    Emitter.call(this);
    var continueWith = null;
    var RegResult = function(){
        var result = {
            success : false,
            message: null,
            user : null,
            log: null
        };
        return result;
    };// Конструктор бъекта, хранящий результаты регистрации
    var validateInputs = function (app) {
        if (!app.email||!app.password){
            app.setInvalid("Email and Password are required");
            self.emit("invalid",app);
        } else if (app.password!==app.confirm){
            app.setInvalid("Confirmation do not match");
            self.emit("invalid",app);
        } else {
            app.validate("Ok");
            self.emit("validated",app);
        }
    };// Проверка введённых данных
    var checkIfUserExists = function (app) {
        User.find({email: app.email},function(err, person){
            assert.ok(err === null);
            if (person.length !== 0){// Адрес найден
                app.setInvalid("Email already exists");
                self.emit("invalid",app);
            } else {// Адрес не найден
                self.emit("user-dosnt-exist",app)
            }
        });
    };// Проверка уникальности адреса
    var cteateUser = function(app){
        var user = User.newUser(app);// Создаю нового пользователя
        user.hashedPassword = bcrypt.hashSync(app.password);// Хэширую его пароль
        user.status = 'approved';
        user.signInCount = 1;
        //console.log('Проверка, user='.yellow, user);
        user.save(function(err){
            assert.ok(err === null,err);
            app.user = user;
            self.emit("user-created",app);
        });// Сохраняю пользователя в БД
    };// Создание и сохранение пользователя
    var addLogEntry = function (app) {
        var log = Log.newRecord({
            subject: "Registration",
            userId: app.user.id,
            entry: "Пользователь зарегистрирован"
        });
        log.save(function (err) {
            assert.ok(err === null, err);
            app.log = log;
            self.emit("log-created",app);
        });
    };// Новая запись в логе
    var registrationOk = function(app){
        var regResult = new RegResult();
        regResult.user = app.user;
        regResult.log = app.log;
        regResult.success = true;
        regResult.message = "Welcome!";
        self.emit("registered",regResult);// сигнал наружу!!!
        if (continueWith){
            continueWith(null,regResult);
        }
    };// Возврат с подтверждением
    var registrationNotOk = function (app) {
        var regResult = new RegResult();
        regResult.user = app.user;
        regResult.log = app.log;
        regResult.success = false;
        regResult.message = app.message;
        self.emit("not-registered",regResult);// сигнал наружу!!!
        if (continueWith){
            continueWith(null,regResult);
        }
    };// Возврат с отказом
    self.applyForMembership = function(args,next){
        continueWith = next;
        var app = new Application(args);
        self.emit("application-received",app);
    };
    self.on("application-received", validateInputs);
    self.on("validated", checkIfUserExists);
    self.on("user-dosnt-exist", cteateUser);
    self.on("user-created", addLogEntry);
    self.on("log-created",registrationOk);
    self.on("invalid",registrationNotOk);

    return self;
};
util.inherits(Registration,Emitter);
module.exports = Registration;
// Модуль регистрации создаёт объект с результатами регистрации,
// обладающий методом 'applyForMembership', который на основе полученных
// от пользователя данных создаёт следующие объекты:
// Заявление (проверяя валидность данных),
// Пользователь (проверив дублирование логина в БД),
// Запись_В_Логе
