#  Membership module for [node](http://nodejs.org)

## Installation

after adding module dependency to your main progect.json file, run

```bash
$ npm install
```

## Features

  * Authenticate users
  * Register users
  * Finding user by token

## How To Use

```bash
Модуль использует mongoose-соединение, установленное из главного
модуля приложения, которое необходимо передать через глобальную
переменную global.
На данный момент модуль предоставляет 4 метода:
authenticate(x,y,next)
registrate(x,y,z,next)
getUserByToken(token,next)
connect()

```

## Examples

```bash
var loginStrategy = new LocalStrategy(idField, function(email, password, done){
  membership.authenticate(email, password, function(err, authResult){
    if (authResult.success){
      done(null,authResult.user,{message:authResult.message});
    } else {
      done(null,false,{message:authResult.message});
    }
  })
});
var registerStrategy = new LocalStrategy(idField, function(email, password, confirm, done){
  membership.register(email, password, confirm, function(err, regResult){
    if (regResult.success){
      done(null,regResult.user,{message:regResult.message});
    } else {
      done(null,false,{message:regResult.message});
    }
  })
});

```

## Tests

  To run the tests, first install the dependencies, then install and run `mocha`:

```bash
$ mocha
```

## People

The author of this module is Anton Pegov (antonpegov@gmail.com)
Original idea came from Robert Conery (robconery@gmail.com)

## License

This is private module of Anton Pegov